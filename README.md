# Результат работы

 - profiles.yml сконфигурирован
 - DataGrip подключен
 - external tables to S3 - загружены
- sources.yml - прописаны
- base tabes - потроены
- wide table - сконфигурирована
- tests and docs for models - пройдены

## Fail
- Q2.1 - Пусто
- Q3.3 - Пусто
- Q4.2 - Пусто

## Команды
docker ps

docker exec -it e471d09aba6d /bin/bash

dbt init


dbt bebug

![](/img/debug.png)


dbt run-operation init_s3_sources

![](/img/sources.png)

dbt build -s tag:staging

![](/img/staging.png)


dbt build -s f_lineorder_flat

![](/img/flat.png)

Query 1..3
![](/img/query.png)
