{% macro init_s3_sources() -%}

    {% set sources = [
        'DROP TABLE IF EXISTS src_customer'
        , 'CREATE TABLE IF NOT EXISTS src_customer
        (
                C_CUSTKEY       UInt32,
                C_NAME          String,
                C_ADDRESS       String,
                C_CITY          LowCardinality(String),
                C_NATION        LowCardinality(String),
                C_REGION        LowCardinality(String),
                C_PHONE         String,
                C_MKTSEGMENT    LowCardinality(String)
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/dbgen/customer.tbl\', \'CSV\')      
        '
        , 'DROP TABLE IF EXISTS src_orders'
        , 'CREATE TABLE src_orders
        (
            O_ORDERKEY             UInt32,
            O_CUSTKEY              UInt32,
            O_ORDERSTATUS            LowCardinality(String),            
            O_TOTALPRICE             Decimal(15,2),     
            O_ORDERDATE            Date,
            O_ORDERPRIORITY            LowCardinality(String),
            O_CLERK          String,
            O_SHIPPRIORITY           UInt8,  
            O_COMMENT               String
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/tpch-dbgen-1g/orders.tbl\', \'CustomSeparated\')
        SETTINGS
            format_custom_field_delimiter=\'|\'
            ,format_custom_escaping_rule=\'CSV\'
            ,format_custom_row_after_delimiter=\'|\n\'                
        '
        , 'DROP TABLE IF EXISTS src_lineitem'
        , 'CREATE TABLE src_lineitem
        (
            L_ORDERKEY             UInt32,
            L_PARTKEY              UInt32,
            L_SUPPKEY              UInt32,
            L_LINENUMBER           UInt8,
            L_QUANTITY             Decimal(15,2),
            L_EXTENDEDPRICE        Decimal(15,2),            
            L_DISCOUNT             Decimal(15,2),
            L_TAX                  Decimal(15,2),
            L_RETURNFLAG            LowCardinality(String),
            L_LINESTATUS            LowCardinality(String),
            L_SHIPDATE              Date,
            L_COMMITDATE            Date,
            L_RECEIPTDATE           Date,            
            L_SHIPINSTRUCT          String,
            L_SHIPMODE             LowCardinality(String),
            L_COMMENT               String
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/tpch-dbgen-1g/lineitem.tbl\', \'CustomSeparated\')
        SETTINGS
            format_custom_field_delimiter=\'|\'
            ,format_custom_escaping_rule=\'CSV\'
            ,format_custom_row_after_delimiter=\'|\n\'                
        '
        , 'DROP TABLE IF EXISTS src_part'
        , 'CREATE TABLE src_part
        (
                P_PARTKEY       UInt32,
                P_NAME          String,
                P_MFGR          LowCardinality(String),
                P_CATEGORY      LowCardinality(String),
                P_BRAND         LowCardinality(String),
                P_COLOR         LowCardinality(String),
                P_TYPE          LowCardinality(String),
                P_SIZE          UInt8,
                P_CONTAINER     LowCardinality(String)
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/dbgen/part.tbl\', \'CSV\')
        '
        , 'DROP TABLE IF EXISTS src_lineorder'
        ,'CREATE TABLE src_lineorder
        (
            LO_ORDERKEY             UInt32,
            LO_LINENUMBER           UInt8,
            LO_CUSTKEY              UInt32,
            LO_PARTKEY              UInt32,
            LO_SUPPKEY              UInt32,
            LO_ORDERDATE            Date,
            LO_ORDERPRIORITY        LowCardinality(String),
            LO_SHIPPRIORITY         UInt8,
            LO_QUANTITY             UInt8,
            LO_EXTENDEDPRICE        UInt32,
            LO_ORDTOTALPRICE        UInt32,
            LO_DISCOUNT             UInt8,
            LO_REVENUE              UInt32,
            LO_SUPPLYCOST           UInt32,
            LO_TAX                  UInt8,
            LO_COMMITDATE           Date,
            LO_SHIPMODE             LowCardinality(String)
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/dbgen/lineorder.tbl\', \'CSV\')
        '



        , 'DROP TABLE IF EXISTS src_supplier'
        , 'CREATE TABLE src_supplier
        (
                S_SUPPKEY       UInt32,
                S_NAME          String,
                S_ADDRESS       String,
                S_CITY          LowCardinality(String),
                S_NATION        LowCardinality(String),
                S_REGION        LowCardinality(String),
                S_PHONE         String
        )
        ENGINE = S3(\'https://storage.yandexcloud.net/otus-dwh/dbgen/supplier.tbl\', \'CSV\')
        '
    ] %}

    {% for src in sources %}
        {% set statement = run_query(src) %}
    {% endfor %}

{{ print('Initialized source tables – TPCH (S3)') }}    

{%- endmacro %}