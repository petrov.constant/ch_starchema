{{
    config (
      engine='MergeTree()',
      order_by=['S_SUPPKEY']
    )
}}

SELECT
                S_SUPPKEY       ,
                S_NAME          ,
                S_ADDRESS       ,
                S_CITY          ,
                S_NATION        ,
                S_REGION        ,
                S_PHONE         
FROM {{ source('dbgen', 'supplier') }}